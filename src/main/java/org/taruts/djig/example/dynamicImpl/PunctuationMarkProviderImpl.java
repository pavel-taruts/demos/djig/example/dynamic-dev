package org.taruts.djig.example.dynamicImpl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.taruts.djig.example.dynamicApi.dynamic.PunctuationMarkProvider;

@Component
public class PunctuationMarkProviderImpl implements PunctuationMarkProvider {

    @Value("${app.punctuation-mark}")
    private String punctuationMark;

    @Override
    public String getPunctuationMark() {
        return punctuationMark;
    }
}
