package org.taruts.djig.example.dynamicImpl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.taruts.djig.example.dynamicApi.dynamic.NameProvider;

@Component
public class NameProviderImpl implements NameProvider {

    @Value("${app.name}")
    private String name;

    @Override
    public String getName() {
        return name;
    }
}
