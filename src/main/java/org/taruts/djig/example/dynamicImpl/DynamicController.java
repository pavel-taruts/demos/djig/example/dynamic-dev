package org.taruts.djig.example.dynamicImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.taruts.djig.example.dynamicApi.dynamic.MessageProvider;

@RestController
@RequestMapping("dynamic-controller")
public class DynamicController {

    @Autowired
    MessageProvider messageProvider;

    @GetMapping("message")
    String getMessage() {
        return messageProvider.getMessage();
    }
}
