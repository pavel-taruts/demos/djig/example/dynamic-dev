package org.taruts.djig.example.dynamicImpl;

import org.apache.commons.numbers.complex.Complex;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.taruts.djig.example.dynamicApi.dynamic.GreetingProvider;
import org.taruts.djig.example.dynamicApi.dynamic.MessageProvider;
import org.taruts.djig.example.dynamicApi.dynamic.NameProvider;
import org.taruts.djig.example.dynamicApi.dynamic.PunctuationMarkProvider;
import org.taruts.djig.example.dynamicApi.main.MessagePrefixProvider;

@Component
public class MessageProviderImpl implements MessageProvider {

    @Autowired
    private MessagePrefixProvider messagePrefixProvider;

    @Autowired
    private GreetingProvider greetingProvider;

    @Autowired
    private NameProvider nameProvider;

    @Autowired
    private PunctuationMarkProvider punctuationMarkProvider;

    @Override
    public String getMessage() {

        // Using the commons-numbers-complex dependency that the dynamic project has but the main app does not
        Complex negativeValue = Complex.ofCartesian(-16, 0);
        Complex sqrtResult = negativeValue.sqrt();

        return messagePrefixProvider.getMessagePrefix() + " " +
               greetingProvider.getGreeting() + ", " +
               nameProvider.getName() +
               punctuationMarkProvider.getPunctuationMark() + " " +
               "Did you know, that the square root of -16 is " + sqrtResult + "?";
    }
}
