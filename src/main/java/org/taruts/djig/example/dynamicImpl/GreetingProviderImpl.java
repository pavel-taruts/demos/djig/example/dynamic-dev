package org.taruts.djig.example.dynamicImpl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.taruts.djig.example.dynamicApi.dynamic.GreetingProvider;

@Component
public class GreetingProviderImpl implements GreetingProvider {

    @Value("${app.greeting}")
    private String greeting;

    @Override
    public String getGreeting() {
        return greeting;
    }
}
